package com.example.memory

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.memory.databinding.ActivityScorelistBinding
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_scorelist.*



class ScorelistActivity : AppCompatActivity() {
    private lateinit var dbref:DatabaseReference
    private lateinit var scoreRecyclerView: RecyclerView
    private lateinit var scoreArrayList: ArrayList<Score>
    private lateinit var binding: ActivityScorelistBinding
    var id=""
    var lvl=0
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding= ActivityScorelistBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.logoutBtn.setOnClickListener {
            Firebase.auth.signOut()
            saveData("UserIdSharedPref","")
            saveDataInt("ScoreSharedPref",0)
            saveDataInt("LevelSharedPref",1)
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

        val actionBar=supportActionBar
        actionBar!!.hide()

        id= intent.getStringExtra("id").toString()
        lvl= intent.getIntExtra("level",1)


//        scoreRecyclerView=scoreList
        scoreRecyclerView=findViewById(R.id.scoreList)
        scoreRecyclerView.layoutManager=LinearLayoutManager(this)
        scoreRecyclerView.setHasFixedSize(true)
        scoreArrayList= arrayListOf<Score>()

        getScoreData()
    }

    private fun getScoreData() {

        dbref= FirebaseDatabase.getInstance("https://memorygame-443e4-default-rtdb.europe-west1.firebasedatabase.app").getReference("Users")
        dbref.orderByChild("score").addValueEventListener(object :ValueEventListener{

            override fun onDataChange(snapshot: DataSnapshot) {
                scoreArrayList.clear()
                if(snapshot.exists()){
                    for(scoreSnapshot in snapshot.children){
                        val points=scoreSnapshot.getValue(Score::class.java)
                        scoreArrayList.add(points!!)
                    }
                    scoreArrayList.reverse()
                    scoreRecyclerView.adapter=MyAdapter(scoreArrayList)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }


        })
//        dbref.addValueEventListener(object :ValueEventListener{
//
//        })
    }

    fun goToLevels(view: View) {
            val activity3 = Intent(this, ChooseActivity::class.java)

            activity3.putExtra("id",id)
            activity3.putExtra("level",lvl)
            startActivity(activity3)
            finish()

    }

    private fun saveData(identyficator : String, key:String){
        val sharedPreferences=getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        val editor=sharedPreferences.edit()
        editor.apply{
            putString(identyficator,key)
        }.apply()
    }
    private fun saveDataInt(identyficator : String, key:Int){
        val sharedPreferences=getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        val editor=sharedPreferences.edit()
        editor.apply{
            putInt(identyficator,key)
        }.apply()
    }
}