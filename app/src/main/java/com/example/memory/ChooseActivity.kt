package com.example.memory

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_choose.*
import com.example.memory.R.color.*


class ChooseActivity : AppCompatActivity() {

    companion object{
        const val LAST_LEVEL = 30;
    }

    var id=""
    var level=1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose)
        loadData()

        val actionBar=supportActionBar
        actionBar!!.hide()

        id = intent.getStringExtra("id").toString()
         level = intent.getIntExtra("level",1)
       // Toast.makeText(this, id, Toast.LENGTH_SHORT).show()
        if(checkForInternet(this)){
            rankingButton.visibility=View.VISIBLE
        }else{
            rankingButton.visibility=View.GONE
        }
        var playerLevel = level-1
        //var playerLevel = 0;

        if(playerLevel>= LAST_LEVEL){
            playerLevel = LAST_LEVEL-1;
        }

        val buttons = arrayOf(
            level1,
            level2,
            level3,
            level4,
            level5,
            level6,
            level7,
            level8,
            level9,
            level10,
            level11,
            level12,
            level13,
            level14,
            level15,
            level16,
            level17,
            level18,
            level19,
            level20,
            level21,
            level22,
            level23,
            level24,
            level25,
            level26,
            level27,
            level28,
            level29,
            level30,
        );

        for (i in 0..playerLevel){
            setActive(buttons[i])
        }

    }

    fun goToLevel(view: View) {

        if (view is Button) {
            val number = view.text;
            val activity2 = Intent(this, LevelActivity::class.java)
            //val activity2 = Intent(this, ScorelistActivity::class.java)
            activity2.putExtra("LEVEL", number.toString())
            activity2.putExtra("id",id)
            //  Toast.makeText(this, number.toString(), Toast.LENGTH_SHORT).show();
            startActivity(activity2)
        }

    }


    fun setActive(button: Button) {
        button.isClickable = true
        button.getBackground().setColorFilter(ContextCompat.getColor(this, backgroundColor2),PorterDuff.Mode.MULTIPLY);
    }

    fun goToRanking(view: View) {

        val activity3 = Intent(this, ScorelistActivity::class.java)
        activity3.putExtra("id", id)
        activity3.putExtra("level",level )
        //  Toast.makeText(this, number.toString(), Toast.LENGTH_SHORT).show();

        startActivity(activity3)
        finish()
    }

    private fun checkForInternet(context: Context):Boolean{
        val connectivityManager=context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){
            val network=connectivityManager.activeNetwork ?: return false
            val activeNetwork=connectivityManager.getNetworkCapabilities(network) ?: return false

            return when{
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ->true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)-> true

                else ->false
            }
        }else{
            @Suppress("Depraction") val networkInfo=connectivityManager.activeNetworkInfo ?: return false
            @Suppress("Depracton") return networkInfo.isConnected
        }
    }
    private fun saveData(identyficator : String, key:String){
        val sharedPreferences=getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        val editor=sharedPreferences.edit()
        editor.apply{
            putString(identyficator,key)
        }.apply()
    }
    private fun saveDataInt(identyficator : String, key:Int){
        val sharedPreferences=getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        val editor=sharedPreferences.edit()
        editor.apply{
            putInt(identyficator,key)
        }.apply()
    }

    private fun loadData(){
        val sharedPreferences=getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        id= sharedPreferences.getString("UserIdSharedPref",null).toString()
        level=sharedPreferences.getInt("LevelSharedPref",1)
    }
}