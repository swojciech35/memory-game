package com.example.memory

class SetsOfLevels {
    companion object {
        var setMini: MutableList<Int> =
            mutableListOf(
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat1,
                R.drawable.cat2,

                );

        var setLevel2: MutableList<Int> =
            mutableListOf(
                R.drawable.cat1,
                R.drawable.ha12,
                R.drawable.cat1,
                R.drawable.ha12,
                R.drawable.cat4,
                R.drawable.cat4

                );

        var setLevel3:MutableList<Int> =
            mutableListOf(
                R.drawable.cat5,
                R.drawable.cat5,
                R.drawable.cat6,
                R.drawable.cat6,
                R.drawable.cat4,
                R.drawable.cat4,
                R.drawable.cat3,
                R.drawable.cat3

            );
        var setLevel4:MutableList<Int> =
            mutableListOf(
                R.drawable.cat1,
                R.drawable.ha9,
                R.drawable.cat2,
                R.drawable.cat8,
                R.drawable.ha9,
                R.drawable.cat1,
                R.drawable.cat8,
                R.drawable.cat2

            );

        var set1: MutableList<Int> =
            mutableListOf(
                R.drawable.ha1,
                R.drawable.cat2,
                R.drawable.cat3,
                R.drawable.cat4,
                R.drawable.ha5,
                R.drawable.cat4,
                R.drawable.ha1,
                R.drawable.cat2,
                R.drawable.cat3,
                R.drawable.ha5

            );
        var set2: MutableList<Int> =
            mutableListOf(
                R.drawable.cat10,
                R.drawable.cat9,
                R.drawable.cat6,
                R.drawable.cat8,
                R.drawable.cat7,
                R.drawable.cat6,
                R.drawable.cat10,
                R.drawable.cat9,
                R.drawable.cat8,
                R.drawable.cat7

            );
        var set3: MutableList<Int> =
            mutableListOf(
                R.drawable.cat1,
                R.drawable.ha2,
                R.drawable.cat3,
                R.drawable.ha4,
                R.drawable.cat5,
                R.drawable.ha6,
                R.drawable.cat1,
                R.drawable.ha2,
                R.drawable.cat3,
                R.drawable.ha4,
                R.drawable.cat5,
                R.drawable.ha6
            );
        var set4: MutableList<Int> =
            mutableListOf(
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.cat14,
                R.drawable.cat15,
                R.drawable.cat16,
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.cat14,
                R.drawable.cat15,
                R.drawable.cat16
            );
        var set5: MutableList<Int> =
            mutableListOf(
                R.drawable.cat10,
                R.drawable.cat9,
                R.drawable.cat8,
                R.drawable.cat7,
                R.drawable.cat5,
                R.drawable.cat6,
                R.drawable.cat10,
                R.drawable.cat5,
                R.drawable.cat8,
                R.drawable.cat7,
                R.drawable.cat9,
                R.drawable.cat6
            );
        var set6: MutableList<Int> =
            mutableListOf(
                R.drawable.ha11,
                R.drawable.cat2,
                R.drawable.ha3,
                R.drawable.cat4,
                R.drawable.ha7,
                R.drawable.cat8,
                R.drawable.cat9,
                R.drawable.cat9,
                R.drawable.ha11,
                R.drawable.cat2,
                R.drawable.ha3,
                R.drawable.cat4,
                R.drawable.ha7,
                R.drawable.cat8,
                R.drawable.cat5,
                R.drawable.cat5
            );
        var set7: MutableList<Int> =
            mutableListOf(
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.cat14,
                R.drawable.cat10,
                R.drawable.cat6,
                R.drawable.cat10,
                R.drawable.cat6,
                R.drawable.cat15,
                R.drawable.cat16,
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.cat14,
                R.drawable.cat15,
                R.drawable.cat16
            );
        var set8: MutableList<Int> =
            mutableListOf(
                R.drawable.cat2,
                R.drawable.cat2,
                R.drawable.cat4,
                R.drawable.cat6,
                R.drawable.cat8,
                R.drawable.cat10,
                R.drawable.cat12,
                R.drawable.cat14,
                R.drawable.cat16,
                R.drawable.cat4,
                R.drawable.cat6,
                R.drawable.cat8,
                R.drawable.cat10,
                R.drawable.cat12,
                R.drawable.cat14,
                R.drawable.cat16
            );
        var set9: MutableList<Int> =
            mutableListOf(
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat10,
                R.drawable.cat10,
                R.drawable.cat3,
                R.drawable.cat4,
                R.drawable.cat7,
                R.drawable.cat8,
                R.drawable.cat9,
                R.drawable.cat9,
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat3,
                R.drawable.cat4,
                R.drawable.cat7,
                R.drawable.cat8,
                R.drawable.cat5,
                R.drawable.cat5
            );
        var set10: MutableList<Int> =
            mutableListOf(
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat4,
                R.drawable.cat6,
                R.drawable.cat8,
                R.drawable.cat10,
                R.drawable.cat12,
                R.drawable.cat14,
                R.drawable.cat16,
                R.drawable.cat4,
                R.drawable.cat6,
                R.drawable.cat8,
                R.drawable.cat10,
                R.drawable.cat12,
                R.drawable.cat14,
                R.drawable.cat16
            );
        var set11: MutableList<Int> =
            mutableListOf(
                R.drawable.cat1,
                R.drawable.cat4,
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.cat14,
                R.drawable.cat10,
                R.drawable.cat1,
                R.drawable.cat10,
                R.drawable.cat4,
                R.drawable.cat15,
                R.drawable.cat16,
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.cat14,
                R.drawable.cat15,
                R.drawable.cat16
            );
        var set12: MutableList<Int> =
            mutableListOf(
                R.drawable.cat3,
                R.drawable.cat4,
                R.drawable.cat5,
                R.drawable.cat6,
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat7,
                R.drawable.cat8,
                R.drawable.cat9,
                R.drawable.cat10,
                R.drawable.cat3,
                R.drawable.cat8,
                R.drawable.cat9,
                R.drawable.cat6,
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat10,
                R.drawable.cat4,
                R.drawable.cat5,
                R.drawable.cat7
            );
        var set13: MutableList<Int> =
            mutableListOf(
                R.drawable.cat13,
                R.drawable.cat4,
                R.drawable.cat15,
                R.drawable.cat6,
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.cat14,
                R.drawable.cat15,
                R.drawable.cat16,
                R.drawable.cat3,
                R.drawable.cat4,
                R.drawable.cat5,
                R.drawable.cat6,
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat3,
                R.drawable.cat14,
                R.drawable.cat5,
                R.drawable.cat16
            );
        var set14: MutableList<Int> =
            mutableListOf(
                R.drawable.cat2,
                R.drawable.cat9,
                R.drawable.cat8,
                R.drawable.cat6,
                R.drawable.cat10,
                R.drawable.cat12,
                R.drawable.cat7,
                R.drawable.cat14,
                R.drawable.cat8,
                R.drawable.cat16,
                R.drawable.cat12,
                R.drawable.cat14,
                R.drawable.cat5,
                R.drawable.cat6,
                R.drawable.cat10,
                R.drawable.cat2,
                R.drawable.cat7,
                R.drawable.cat9,
                R.drawable.cat5,
                R.drawable.cat16
            );
        var set15: MutableList<Int> =
            mutableListOf(
                R.drawable.ha1,
                R.drawable.cat2,
                R.drawable.ha3,
                R.drawable.cat4,
                R.drawable.ha5,
                R.drawable.cat6,
                R.drawable.ha1,
                R.drawable.cat2,
                R.drawable.ha3,
                R.drawable.cat4,
                R.drawable.ha5,
                R.drawable.cat6,
                R.drawable.ha11,
                R.drawable.ha4,
                R.drawable.cat13,
                R.drawable.ha4,
                R.drawable.cat13,
                R.drawable.ha12,
                R.drawable.ha11,
                R.drawable.cat9,
                R.drawable.cat11,
                R.drawable.cat9,
                R.drawable.cat11,
                R.drawable.ha12
            );
        var set16: MutableList<Int> =
            mutableListOf(
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat3,
                R.drawable.cat4,
                R.drawable.cat5,
                R.drawable.cat6,
                R.drawable.ha1,
                R.drawable.ha2,
                R.drawable.ha3,
                R.drawable.ha4,
                R.drawable.ha5,
                R.drawable.ha6,
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat3,
                R.drawable.cat4,
                R.drawable.cat5,
                R.drawable.ha1,
                R.drawable.ha2,
                R.drawable.ha3,
                R.drawable.ha4,
                R.drawable.ha5,
                R.drawable.cat6,
                R.drawable.ha6


            );
        var set17: MutableList<Int> =
            mutableListOf(
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.cat14,
                R.drawable.cat15,
                R.drawable.cat16,
                R.drawable.ha7,
                R.drawable.ha8,
                R.drawable.ha9,
                R.drawable.ha10,
                R.drawable.ha11,
                R.drawable.ha12,
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.cat14,
                R.drawable.cat15,
                R.drawable.cat16,
                R.drawable.ha7,
                R.drawable.ha8,
                R.drawable.ha9,
                R.drawable.ha10,
                R.drawable.ha11,
                R.drawable.ha12

            );
        var set18: MutableList<Int> =
            mutableListOf(
                R.drawable.ha1,
                R.drawable.ha1,
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.cat14,
                R.drawable.cat15,
                R.drawable.cat16,
                R.drawable.ha7,
                R.drawable.ha8,
                R.drawable.ha9,
                R.drawable.ha10,
                R.drawable.ha11,
                R.drawable.ha12,
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.cat14,
                R.drawable.cat15,
                R.drawable.cat16,
                R.drawable.ha7,
                R.drawable.ha8,
                R.drawable.ha9,
                R.drawable.ha10,
                R.drawable.ha6,
                R.drawable.ha6,
                R.drawable.ha11,
                R.drawable.ha12
            );
        var set19: MutableList<Int> =
            mutableListOf(
                R.drawable.cat1,
                R.drawable.ha12,
                R.drawable.cat10,
                R.drawable.cat10,
                R.drawable.cat3,
                R.drawable.ha11,
                R.drawable.cat5,
                R.drawable.cat6,
                R.drawable.ha1,
                R.drawable.ha2,
                R.drawable.ha3,
                R.drawable.cat11,
                R.drawable.cat11,
                R.drawable.ha4,
                R.drawable.ha5,
                R.drawable.ha6,
                R.drawable.cat1,
                R.drawable.ha12,
                R.drawable.cat3,
                R.drawable.ha11,
                R.drawable.cat5,
                R.drawable.cat6,
                R.drawable.ha1,
                R.drawable.ha2,
                R.drawable.ha3,
                R.drawable.ha4,
                R.drawable.ha5,
                R.drawable.ha6
            );
        var set20: MutableList<Int> =
            mutableListOf(
                R.drawable.ha1,
                R.drawable.cat2,
                R.drawable.ha8,
                R.drawable.ha8,
                R.drawable.ha3,
                R.drawable.cat4,
                R.drawable.ha5,
                R.drawable.cat6,
                R.drawable.ha1,
                R.drawable.cat2,
                R.drawable.ha3,
                R.drawable.cat4,
                R.drawable.cat15,
                R.drawable.ha5,
                R.drawable.cat6,
                R.drawable.ha11,
                R.drawable.cat16,
                R.drawable.cat16,
                R.drawable.ha4,
                R.drawable.ha4,
                R.drawable.cat15,
                R.drawable.ha12,
                R.drawable.ha11,
                R.drawable.cat9,
                R.drawable.cat11,
                R.drawable.cat9,
                R.drawable.cat11,
                R.drawable.ha12
            );
        var set21: MutableList<Int> =
            mutableListOf(
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat10,
                R.drawable.cat10,
                R.drawable.cat3,
                R.drawable.cat4,
                R.drawable.cat5,
                R.drawable.cat8,
                R.drawable.ha1,
                R.drawable.ha2,
                R.drawable.ha3,
                R.drawable.cat11,
                R.drawable.ha12,
                R.drawable.cat11,
                R.drawable.ha4,
                R.drawable.ha5,
                R.drawable.ha6,
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat3,
                R.drawable.ha12,
                R.drawable.cat8,
                R.drawable.cat4,
                R.drawable.cat5,
                R.drawable.ha1,
                R.drawable.ha2,
                R.drawable.ha3,
                R.drawable.ha4,
                R.drawable.ha5,
                R.drawable.ha6
            );
        var set22: MutableList<Int> =
            mutableListOf(
                R.drawable.ha1,
                R.drawable.cat2,
                R.drawable.ha8,
                R.drawable.ha8,
                R.drawable.ha3,
                R.drawable.cat4,
                R.drawable.ha5,
                R.drawable.cat6,
                R.drawable.ha1,
                R.drawable.cat2,
                R.drawable.ha12,
                R.drawable.ha3,
                R.drawable.cat4,
                R.drawable.ha5,
                R.drawable.cat6,
                R.drawable.ha11,
                R.drawable.cat16,
                R.drawable.ha12,
                R.drawable.cat16,
                R.drawable.ha4,
                R.drawable.cat13,
                R.drawable.ha4,
                R.drawable.cat15,
                R.drawable.ha12,
                R.drawable.ha11,
                R.drawable.cat9,
                R.drawable.cat11,
                R.drawable.cat9,
                R.drawable.cat11,
                R.drawable.ha12
            );
        var set23: MutableList<Int> =
            mutableListOf(
                R.drawable.ha1,
                R.drawable.ha1,
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.cat14,
                R.drawable.cat15,
                R.drawable.cat16,
                R.drawable.ha7,
                R.drawable.ha2,
                R.drawable.ha9,
                R.drawable.ha10,
                R.drawable.ha8,
                R.drawable.ha11,
                R.drawable.ha12,
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.ha6,
                R.drawable.cat14,
                R.drawable.cat15,
                R.drawable.cat16,
                R.drawable.ha7,
                R.drawable.ha8,
                R.drawable.ha9,
                R.drawable.ha10,
                R.drawable.ha2,
                R.drawable.ha6,
                R.drawable.ha11,
                R.drawable.ha12
            );
        var set24: MutableList<Int> =
            mutableListOf(
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat10,
                R.drawable.cat10,
                R.drawable.cat3,
                R.drawable.cat4,
                R.drawable.cat5,
                R.drawable.cat6,
                R.drawable.cat15,
                R.drawable.ha6,
                R.drawable.cat1,
                R.drawable.cat2,
                R.drawable.cat3,
                R.drawable.ha12,
                R.drawable.cat4,
                R.drawable.cat5,
                R.drawable.ha1,
                R.drawable.ha2,
                R.drawable.ha3,
                R.drawable.cat15,
                R.drawable.ha1,
                R.drawable.ha2,
                R.drawable.cat6,
                R.drawable.ha3,
                R.drawable.cat11,
                R.drawable.ha12,
                R.drawable.cat11,
                R.drawable.ha4,
                R.drawable.ha5,
                R.drawable.ha4,
                R.drawable.ha5,
                R.drawable.ha6
            );
        var set25: MutableList<Int> =
            mutableListOf(
                R.drawable.ha3,
                R.drawable.cat11,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.ha4,
                R.drawable.ha8,
                R.drawable.ha11,
                R.drawable.ha12,
                R.drawable.cat11,
                R.drawable.ha4,
                R.drawable.cat12,
                R.drawable.cat13,
                R.drawable.ha6,
                R.drawable.cat14,
                R.drawable.cat15,
                R.drawable.cat14,
                R.drawable.ha3,
                R.drawable.cat15,
                R.drawable.cat16,
                R.drawable.ha7,
                R.drawable.ha2,
                R.drawable.ha9,
                R.drawable.ha10,
                R.drawable.cat16,
                R.drawable.ha7,
                R.drawable.ha8,
                R.drawable.ha9,
                R.drawable.ha10,
                R.drawable.ha2,
                R.drawable.ha6,
                R.drawable.ha11,
                R.drawable.ha12
            );

        var setMax: MutableList<Int> =
            mutableListOf(
                R.drawable.ha1,
                R.drawable.cat2,
                R.drawable.ha8,
                R.drawable.ha8,
                R.drawable.ha3,
                R.drawable.cat4,
                R.drawable.ha10,
                R.drawable.ha10,
                R.drawable.ha7,
                R.drawable.cat16,
                R.drawable.ha4,
                R.drawable.cat13,
                R.drawable.ha4,
                R.drawable.cat15,
                R.drawable.ha5,
                R.drawable.cat6,
                R.drawable.ha1,
                R.drawable.cat2,
                R.drawable.ha7,
                R.drawable.ha3,
                R.drawable.cat4,
                R.drawable.ha5,
                R.drawable.cat6,
                R.drawable.ha11,
                R.drawable.cat16,
                R.drawable.ha12,
                R.drawable.ha11,
                R.drawable.cat9,
                R.drawable.cat11,
                R.drawable.cat9,
                R.drawable.cat11,
                R.drawable.ha12
            );







        var levels: MutableList<Level> = mutableListOf(
            Level(setMini, 2, 2, 10, false, false),
            Level(setLevel2,3,2,20,true,true),
            Level(setLevel3,4,2,0,true,true),
            Level(setLevel4,4,2,30,false,true),
            Level(set1, 5,2, 0, true, true),
            Level(set2, 5,2, 40, false, true),
            Level(set3, 4, 3, 0, true, true),
            Level(set4, 4, 3, 0, false, false),
            Level(set5, 4,3, 50, true, false),
            Level(set6, 4,4, 0, true, true),
            Level(set7, 4, 4, 0, false, false),
            Level(set8, 4, 4, 60, true, false),
            Level(set9, 6, 3, 0, true, true),
            Level(set10, 6, 3, 0, false, false),
            Level(set11, 6, 3, 70, true, false),
            Level(set12, 5, 4, 0, true, true),
            Level(set13, 5, 4, 0, false, false),
            Level(set14, 5, 4, 80, true, false),
            Level(set15, 6, 4, 0, true, true),
            Level(set16, 6, 4, 0, false, false),
            Level(set17, 6, 4, 100, true, false),
            Level(set18, 7, 4, 0, true, true),
            Level(set19, 7, 4, 0, false, false),
            Level(set20, 7, 4, 120, false, false),
            Level(set21, 6, 5, 0, true, true),
            Level(set22, 6, 5, 0, false, false),
            Level(set23, 6, 5, 130, true, false),
            Level(set24, 8, 4, 0, true, true),
            Level(set25, 8, 4, 0, false, false),
            Level(setMax, 8, 4, 140, true, false)

        )
    }
}