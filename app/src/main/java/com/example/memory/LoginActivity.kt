package com.example.memory

import android.app.ProgressDialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import com.example.memory.databinding.ActivityLoginBinding
import com.example.memory.databinding.ActivityMainBinding
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignIn.getClient
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.level_fail_layout.*

class LoginActivity : AppCompatActivity() {
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var auth:FirebaseAuth
    private lateinit var actionBar: ActionBar
    private lateinit var progressDialog: ProgressDialog
    private lateinit var binding: ActivityLoginBinding
    private lateinit var database: DatabaseReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityLoginBinding.inflate(layoutInflater)
        val actionBar=supportActionBar
        actionBar!!.hide()
        setContentView(binding.root)
        auth= Firebase.auth

        progressDialog =ProgressDialog(this)
        progressDialog.setTitle("Please wait")
        progressDialog.setMessage("Logging In...")
        progressDialog.setCanceledOnTouchOutside(false)
        if(checkForInternet(this)) {

            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("768655964785-442ep047trqfndjbuqfo90m9vcsq5ge0.apps.googleusercontent.com")
                .requestEmail()
                .build()
            googleSignInClient = GoogleSignIn.getClient(this, gso)

            binding.btnLoginGoogle.setOnClickListener {
                signIn()
            }

            ///////email login


            checkUser()
            binding.registerBtn.setOnClickListener {
                startActivity(Intent(this, SignUpActivity::class.java))
            }
            binding.LoginBtn.setOnClickListener {
                validateData()
            }
        }else{
            Toast.makeText(this, "no internet connection", Toast.LENGTH_SHORT).show()
        }
    }

    private fun validateData() {
        email=binding.emailEt.text.toString().trim()
        password=binding.passwordEt.text.toString().trim()
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            binding.emailEt.error="invalid email format"
        }else if(TextUtils.isEmpty(password)){
            binding.passwordEt.error="Please enter password"
        }else{
            firebaseLogin()
        }
    }

    private fun checkUser() {
        loadData()
        val firebaseUser=auth.currentUser
        if(firebaseUser!=null){
            progressDialog.show()
            database= FirebaseDatabase.getInstance("https://memorygame-443e4-default-rtdb.europe-west1.firebasedatabase.app").getReference("Users")
            database.child(firebaseUser.uid).get().addOnSuccessListener {
                if(it.exists()){

                        if(idSharedPref==it.child("id").value.toString()) {

                        if (scoreSharedPref > Integer.parseInt((it.child("score").value.toString()))) {
                            val User = User(firebaseUser.uid, it.child("nick").value.toString(), it.child("email").value.toString(), lvlSharedPrefs, scoreSharedPref)
                            database.child(firebaseUser.uid).setValue(User).addOnSuccessListener {
                            }.addOnFailureListener {
                                Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
                            }
                        } else {
                            saveDataInt("ScoreSharedPref", Integer.parseInt((it.child("score").value.toString())))
                            saveDataInt("LevelSharedPref", Integer.parseInt((it.child("level").value.toString())))
                        }
                    }else{
//                        Toast.makeText(this, "zalogowano na innego uzytkowanika", Toast.LENGTH_SHORT).show()
                    }
                    saveData("UserIdSharedPref",firebaseUser.uid)
                    progressDialog.dismiss()
                    Toast.makeText(this, "LoggedIn as ${firebaseUser.email}", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this,ChooseActivity::class.java)
                    intent.putExtra("id",(it.child("id").value.toString()))
                    intent.putExtra("level",Integer.parseInt((it.child("level").value.toString())))
                    startActivity(intent)
                    finish()
                }else{
                    createUserInDataBase(firebaseUser.uid,firebaseUser.displayName.toString(),firebaseUser.email.toString())
                    progressDialog.dismiss()
                    val intent = Intent(this,ChooseActivity::class.java)
                    intent.putExtra("id",firebaseUser.uid)
                    intent.putExtra("level",1)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }
    private fun firebaseLogin(){
        progressDialog.show()
        auth.signInWithEmailAndPassword(email,password)
            .addOnSuccessListener {
                val firebaseUser=auth.currentUser
                val email=firebaseUser!!.email
                Toast.makeText(this, "LoggedIn as $email", Toast.LENGTH_SHORT).show()
                database= FirebaseDatabase.getInstance("https://memorygame-443e4-default-rtdb.europe-west1.firebasedatabase.app").getReference("Users")
                database.child(firebaseUser.uid).get().addOnSuccessListener {
                    if(it.exists()){
                        if(idSharedPref==it.child("id").value.toString()) {
                            if (scoreSharedPref > Integer.parseInt((it.child("score").value.toString()))) {
                                val User = User(firebaseUser.uid, it.child("nick").value.toString(), it.child("email").value.toString(), lvlSharedPrefs, scoreSharedPref)
                                database.child(firebaseUser.uid).setValue(User).addOnSuccessListener {
                                }.addOnFailureListener {
                                    Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
                                }
                            } else {
                                saveDataInt("ScoreSharedPref", Integer.parseInt((it.child("score").value.toString())))
                                saveDataInt("LevelSharedPref", Integer.parseInt((it.child("level").value.toString())))
                            }
                        }else{
//                            Toast.makeText(this, "zalogowano na innego uzytkowanika", Toast.LENGTH_SHORT).show()
                        }
                        progressDialog.dismiss()
                        val intent = Intent(this,ChooseActivity::class.java)
                        saveData("UserIdSharedPref",firebaseUser.uid)
                        intent.putExtra("id",(it.child("id").value.toString()))
                        intent.putExtra("level",Integer.parseInt((it.child("level").value.toString())))
                        startActivity(intent)
                        finish()
                    }
                }
            }
            .addOnFailureListener { e->
                progressDialog.dismiss()
                Toast.makeText(this, "login failed fue to ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }
//////////google
    private fun signIn() {
        val signInIntent=googleSignInClient.signInIntent
        startActivityForResult(signInIntent,RC_SIGN_IN)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {

                val account = task.getResult(ApiException::class.java)!!
                Log.d(ContentValues.TAG, "firebaseAuthWithGoogle:" + account.id)
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {

                Log.w(ContentValues.TAG, "Google sign in failed", e)
            }
        }
    }
    private fun firebaseAuthWithGoogle(idToken: String) {

        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {

                    Log.d(ContentValues.TAG, "signInWithCredential:success")
                    val user = auth.currentUser
                    updateUI(user)

                } else {

                    Log.w(ContentValues.TAG, "signInWithCredential:failure", task.exception)
                    updateUI(null)
                }
            }
    }

    private fun updateUI(user: FirebaseUser?) {
        if(user!=null){

            database= FirebaseDatabase.getInstance("https://memorygame-443e4-default-rtdb.europe-west1.firebasedatabase.app").getReference("Users")
            database.child(user.uid).get().addOnSuccessListener {
                if(it.exists()){
                    saveData("UserIdSharedPref",user.uid)
//                    Toast.makeText(this, it.child("nick").value.toString(), Toast.LENGTH_SHORT).show()
                    val intent = Intent(this,ChooseActivity::class.java)
                    intent.putExtra("id",(it.child("id").value.toString()))
                    intent.putExtra("level",Integer.parseInt((it.child("level").value.toString())))
                    startActivity(intent)
                    finish()
                }else{
                    createUserInDataBase(user.uid,user.displayName.toString(),user.email.toString())
                    val intent = Intent(this,ChooseActivity::class.java)
                    intent.putExtra("id",user.uid)
                    intent.putExtra("level",1)
                    startActivity(intent)
                    finish()
                }

            }
        }
    }

    fun createUserInDataBase(id: String,nick:String,email:String){
        saveData("UserIdSharedPref",id)
        database=FirebaseDatabase.getInstance("https://memorygame-443e4-default-rtdb.europe-west1.firebasedatabase.app").getReference("Users")
        val User=User(id,nick,email,1,0)
        database.child(id).setValue(User).addOnSuccessListener {
            Toast.makeText(this, "added succesfully", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this,  "nie dodano", Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkForInternet(context: Context):Boolean{
        val connectivityManager=context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){
            val network=connectivityManager.activeNetwork ?: return false
            val activeNetwork=connectivityManager.getNetworkCapabilities(network) ?: return false

            return when{
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ->true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)-> true

                else ->false
            }
        }else{
            @Suppress("Depraction") val networkInfo=connectivityManager.activeNetworkInfo ?: return false
            @Suppress("Depracton") return networkInfo.isConnected
        }
    }
   private fun saveData(identyficator : String, key:String){
        val sharedPreferences=getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        val editor=sharedPreferences.edit()
        editor.apply{
            putString(identyficator,key)
        }.apply()
    }
    private fun saveDataInt(identyficator : String, key:Int){
        val sharedPreferences=getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        val editor=sharedPreferences.edit()
        editor.apply{
            putInt(identyficator,key)
        }.apply()
    }
    private fun loadData() {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        scoreSharedPref = sharedPreferences.getInt("ScoreSharedPref", 0)
        idSharedPref= sharedPreferences.getString("UserIdSharedPref"," ").toString()
        lvlSharedPrefs=sharedPreferences.getInt("LevelSharedPref",1)
    }
    companion object{
        const val RC_SIGN_IN=1001
        var scoreSharedPref=0
        var idSharedPref=" "
        var lvlSharedPrefs=1
        private var email=""
        private var password=""
    }
}