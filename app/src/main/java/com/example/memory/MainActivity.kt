package com.example.memory

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val SPLASH_SCREEN = 3000
    private lateinit var topAnimation: Animation
    private lateinit var bottomAnimation: Animation


    companion object {
        var id = " "
        var lvl = 1
        var score = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadData()
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_main)
        val actionBar = supportActionBar
        actionBar!!.hide()
        topAnimation = AnimationUtils.loadAnimation(this, R.anim.top_animation)
        bottomAnimation = AnimationUtils.loadAnimation(this, R.anim.bottom_animation)
        memoryImmage.animation = topAnimation
        gameImmage.animation = bottomAnimation

        Handler().postDelayed({
            if (checkForInternet(this)) {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            } else {
                if (id != " ") {
                    Toast.makeText(this, "internet error playing offline", Toast.LENGTH_SHORT)
                        .show()
                    val intent = Intent(this, ChooseActivity::class.java)
                    intent.putExtra("id", id)
                    intent.putExtra("level", lvl)
                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(this, "Please connect to internet", Toast.LENGTH_SHORT).show()
                }
            }
        }, SPLASH_SCREEN.toLong())
    }
    private fun checkForInternet(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            @Suppress("Depraction") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("Depracton") return networkInfo.isConnected
        }
    }
    private fun loadData() {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        id = sharedPreferences.getString("UserIdSharedPref", " ").toString()
        lvl = sharedPreferences.getInt("LevelSharedPref", 1)
        score = sharedPreferences.getInt("ScoreSharedPref", 0)
    }


}