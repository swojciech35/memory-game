package com.example.memory

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MyAdapter(private val scoreList:ArrayList<Score>): RecyclerView.Adapter<MyAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView=LayoutInflater.from(parent.context).inflate(R.layout.score_item,parent,false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentitem=scoreList[position]
        holder.nick.text=currentitem.nick
        holder.points.text= currentitem.score.toString()
        holder.rankPosition.text=(position+1).toString()
    }

    override fun getItemCount(): Int {
        return scoreList.size
    }

    class MyViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        val nick:TextView=itemView.findViewById(R.id.nick)
        val points:TextView=itemView.findViewById(R.id.score)
        val rankPosition:TextView=itemView.findViewById(R.id.position)
    }
}