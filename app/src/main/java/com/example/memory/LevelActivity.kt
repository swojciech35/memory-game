package com.example.memory

import LevelFailDialog
import LevelSuccessDialog
import PauseDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_level.*
import com.example.memory.R.string.*

class LevelActivity : FragmentActivity() {
    private lateinit var database: DatabaseReference
    val dialog = PauseDialog()
    val dialog2 = LevelSuccessDialog()
    val dialog3 = LevelFailDialog()
    var timeWhenStopped: Long = 0
    lateinit var handler: Handler

    companion object {
        var time = "0:00"
        var allMoves = 0
        var lvl=-1
        var id=""
        var userScore=0
        var userEmail=""
        var userNick=""
        var currentLevel=0

        var levelResult = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_level)
        id= intent.getStringExtra("id").toString()
        getLvlAndScore(id)
        val intent = intent
        var level: Level = SetsOfLevels.levels[Integer.parseInt(intent.getStringExtra("LEVEL")) - 1]
        currentLevel = Integer.parseInt(intent.getStringExtra("LEVEL")) - 1
        var playerLevel = lvl
        val images: MutableList<Int> = level.cardSet
        val buttons = arrayOf(
            button,
            button2,
            button3,
            button4,
            button5,
            button6,
            button7,
            button8,
            button9,
            button10,
            button11,
            button12,
            button13,
            button14,
            button15,
            button16,
            button17,
            button18,
            button19,
            button20,
            button21,
            button22,
            button23,
            button24,
            button25,
            button26,
            button27,
            button28,
            button29,
            button30,
            button31,
            button32,
            button33,
            button34,
            button35,
            button36,
            button37,
            button38,
            button39,
            button40
        )
        for (i in buttons.size - 1 downTo images.size) {
            buttons[i].visibility = View.GONE
        }
        tableLayout.rowCount = level.rows
        tableLayout.columnCount = level.columns


        images.shuffle()
        allMoves = 0
        timeWhenStopped = 0
        var pairs = 0
        var clicked = 0
        var cardOne = -1
        var cardTwo = -1
        var pairsToWin = images.size / 2
        levelNumber.setText((currentLevel+1).toString())

        handler = Handler()

        if (level.movesLimit != 0) {
            numberOfMoves.setText(getString(number_of_moves_text) +" " + allMoves + "/" + level.movesLimit.toString())
        }
        else{
            numberOfMoves.setText(getString(number_of_moves_text) +" " + allMoves)
        }


        var newSize = buttons[0].layoutParams.height;

        if (level.columns < 3 && level.rows < 4) {
            newSize = (newSize * 1.5).toInt();
        }

        if (level.columns > 3 || level.rows > 5) {
            newSize = (newSize * 0.7).toInt();
        }

        if (level.columns > 4 || level.rows > 7) {
            newSize = (newSize * 0.8).toInt();
        }

        if (newSize != buttons[0].layoutParams.height) {
            for (i in 0..images.size - 1) {
                buttons[i].layoutParams.height = newSize;
                buttons[i].layoutParams.width = newSize;
            }
        }


        val flipAllCards = object : Runnable {
            override fun run() {
                for (i in 0..images.size - 1) {
                    cardFlip(buttons[i], images[i])
                }
            }
        }

        val game = object : Runnable {
            override fun run() {
                for (i in 0..images.size - 1) {
                    if (level.ifMarkedUnrevealed == true) {
                        buttons[i].setBackgroundResource(R.drawable.cardback_first)
                    } else {
                        buttons[i].setBackgroundResource(R.drawable.cardback)
                    }
                    buttons[i].text = "cardback"
                    buttons[i].setOnClickListener {
                        if (allMoves == 0) {
                            timerResume()
                        }

                        if (buttons[i].text == "cardback") {
                            allMoves++

                            if (level.movesLimit != 0 && allMoves > level.movesLimit) {
                                showLevelFailDialog()
                            }
                            if (level.movesLimit != 0) {
                                numberOfMoves.setText(getString(number_of_moves_text) + " " + allMoves + "/" + level.movesLimit.toString())
                            } else {
                                numberOfMoves.setText(getString(number_of_moves_text) + " " + allMoves)
                            }
                            if (clicked == 0) {
                                if (buttons[i].text == "cardback") {
                                    cardFlip(buttons[i], images[i])
                                    cardOne = i
                                    cardTwo = i
                                    clicked++
                                }
                            } else
                                if (clicked == 1) {
                                    cardFlip(buttons[i], images[i])
                                    if (buttons[i].text == buttons[cardTwo].text) {
                                        buttons[i].isClickable = false
                                        buttons[cardTwo].isClickable = false
                                        pairs++
                                        if (pairs == pairsToWin) {

                                            timerPause()
                                            time = timer.text as String
                                            levelResult=countPoints()
                                            showLevelSuccessDialog()
                                        }
                                        clicked = 0
                                        cardOne = i
                                        cardTwo = i
                                    } else {
                                        cardOne = cardTwo
                                        cardTwo = i
                                        clicked++
                                    }
                                } else if (clicked == 2) {
                                    cardFlip(buttons[i], images[i])
                                    cardFlip(buttons[cardOne], R.drawable.cardback)
                                    cardFlip(buttons[cardTwo], R.drawable.cardback)
                                    cardOne = i
                                    cardTwo = i
                                    clicked = 1
                                }


                        }


                    }
                }

                pauseButton.setOnClickListener {
                    timerPause()
                    showNoticeDialog()


                }
            }

            fun countPoints() : Int{
                var timeInSeconds = ((SystemClock.elapsedRealtime() - timer!!.getBase()) / 1000)
                if (timeInSeconds.toInt() == 0 ) timeInSeconds = 1;
                var result =((currentLevel + 1) * 1000 / (allMoves/level.cardSet.size) / timeInSeconds).toInt() * level.cardSet.size;
                if (level.ifRevealAtStart==false){
                    result =(result * 2).toInt()
                }

                if  (lvl != Integer.parseInt(intent.getStringExtra("LEVEL"))) {
                    result = (result * 0.5).toInt()
                }
             //   Toast.makeText(this, countPoints().toString(), Toast.LENGTH_SHORT).show()

               return result.toInt()
            }


        }

        if (level.ifRevealAtStart == true) {
            //handler.postDelayed(cardSize,0)
            handler.postDelayed(flipAllCards, 0)
            handler.postDelayed(game, 2000)
        } else {
            //handler.postDelayed(cardSize,0)
            handler.postDelayed(game, 0)
        }

    }

    fun showNoticeDialog() {
        // Create an instance of the dialog fragment and show it
        dialog.show(supportFragmentManager, "PauseDialog")
        dialog.isCancelable = false

    }

    fun showLevelSuccessDialog() {

        getLvlAndScore(id)

        if (lvl == Integer.parseInt(intent.getStringExtra("LEVEL"))) {
            updateScore(
                id,
                lvl + 1,
                userScore + levelResult
            )// test update TODO: ogarnac lvl numerki
        }
        else{
            updateScore(
                id,
                lvl,
                userScore + levelResult
            )
        }


        dialog2.show(supportFragmentManager, "LevelSuccessDialogFragment")
        dialog2.isCancelable = false

    }

    fun showLevelFailDialog() {

        dialog3.show(supportFragmentManager, "LevelFailDialogFragment")
        dialog3.isCancelable = false

    }


    fun cardFlip(button: Button, image: Int) {

        button.setBackgroundResource(image)
        button.setText(getResources().getResourceEntryName(image))

    }

    fun timerPause() {
        timeWhenStopped = timer.getBase() - SystemClock.elapsedRealtime()
        timer.stop()
    }

    fun timerResume() {
        timer.setBase(SystemClock.elapsedRealtime() + timeWhenStopped)
        timer.start()
    }

    fun resumeGame(view: View) {
        timerResume()
        dialog.dismiss()
    }

    fun goToNextLevel(view: View) {
        val intent = intent
        val number = Integer.parseInt(intent.getStringExtra("LEVEL")) + 1
        val activity2 = Intent(this, LevelActivity::class.java)
        activity2.putExtra("id", id)
        activity2.putExtra("LEVEL", number.toString())
        activity2.putExtra("level", lvl)
        startActivity(activity2)
        finish()
    }

    fun restartLevel(view: View) {
        val intent = intent
        val number = Integer.parseInt(intent.getStringExtra("LEVEL"))
        val activity2 = Intent(this, LevelActivity::class.java)
        activity2.putExtra("LEVEL", number.toString())
        activity2.putExtra("level", lvl)
        activity2.putExtra("id", id)
        saveDataInt("LevelSharedPref",lvl)
        startActivity(activity2)
        finish()
    }

    fun goToChooseLevel(view: View) {

        val activity2 = Intent(this, ChooseActivity::class.java)
        activity2.putExtra("id",id)
        activity2.putExtra("level", lvl)

        startActivity(activity2)
        finish()
    }


    fun updateScore(id: String, level: Int, score: Int) {
        if(checkForInternet(this)) {
            saveDataInt("ScoreSharedPref",score)
            saveDataInt("LevelSharedPref",level)

            database =
                FirebaseDatabase.getInstance("https://memorygame-443e4-default-rtdb.europe-west1.firebasedatabase.app")
                    .getReference("Users")
            val User = User(id, userNick, userEmail, level, score)
            database.child(id).setValue(User).addOnSuccessListener {
            }.addOnFailureListener {
            }
        }else{
            
            saveDataInt("ScoreSharedPref",score)
            saveDataInt("LevelSharedPref",level)
        }
    }

    fun getLvlAndScore(id:String){
        if(checkForInternet(this)) {
            database = FirebaseDatabase.getInstance("https://memorygame-443e4-default-rtdb.europe-west1.firebasedatabase.app")
                    .getReference("Users")
            database.child(id).get().addOnSuccessListener {
                if (it.exists()) {
                    lvl = Integer.parseInt((it.child("level").value.toString()))
                    userScore = Integer.parseInt((it.child("score").value.toString()))
                    userNick = it.child("nick").value.toString()
                    userEmail = it.child("email").value.toString()

                } else {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                }
            }
        }else{

            loadData()
        }

    }

    private fun checkForInternet(context: Context):Boolean{
        val connectivityManager=context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){
            val network=connectivityManager.activeNetwork ?: return false
            val activeNetwork=connectivityManager.getNetworkCapabilities(network) ?: return false

            return when{
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ->true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)-> true

                else ->false
            }
        }else{
            @Suppress("Depraction") val networkInfo=connectivityManager.activeNetworkInfo ?: return false
            @Suppress("Depracton") return networkInfo.isConnected
        }
    }
    private fun saveDataInt(identyficator : String, key:Int){
        val sharedPreferences=getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        val editor=sharedPreferences.edit()
        editor.apply{
            putInt(identyficator,key)
        }.apply()
    }
    private fun loadData(){
        val sharedPreferences=getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        id= sharedPreferences.getString("UserIdSharedPref",null).toString()
        lvl=sharedPreferences.getInt("LevelSharedPref",1)
        userScore=sharedPreferences.getInt("ScoreSharedPref",0)
    }
}



