
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.example.memory.ChooseActivity.Companion.LAST_LEVEL
import com.example.memory.LevelActivity.Companion.allMoves
import com.example.memory.LevelActivity.Companion.currentLevel
import com.example.memory.LevelActivity.Companion.levelResult
import com.example.memory.LevelActivity.Companion.time
import com.example.memory.R
import com.example.memory.R.string.*

class LevelSuccessDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater;
            val dialogg: View = inflater.inflate(R.layout.level_success_layout,null)
            val tekst = dialogg.findViewById<TextView>(R.id.resultsText);
            tekst.text = getString(time_text) + time + "\n"+getString(number_of_moves_text) +" "+ allMoves.toString() + "\n" +getString(
                points_text) +" "+levelResult.toString();

            if (currentLevel+1==LAST_LEVEL){
                val nextLevel = dialogg.findViewById<Button>(R.id.nextLevelButton);
                nextLevel.setAlpha(0.5f);
                nextLevel.isClickable = false;
            }

            builder.setView(dialogg)

            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")

    }





}