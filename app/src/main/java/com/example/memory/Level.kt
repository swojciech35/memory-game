package com.example.memory

class Level {
    var rows: Int = 0;
    var columns: Int = 0;
    var movesLimit: Int = 0;
    var cardSet: MutableList<Int> = mutableListOf();
    var ifRevealAtStart: Boolean = false;
    var ifMarkedUnrevealed: Boolean = false;


    constructor(
        cardSet: MutableList<Int>,
        rows: Int,
        columns: Int,
        movesLimit: Int,
        ifRevealAtStart: Boolean,
        ifMarkedUnrevealed: Boolean
    ) {
        this.cardSet = cardSet;
        this.rows = rows;
        this.columns = columns;
        this.movesLimit = movesLimit;
        this.ifMarkedUnrevealed = ifMarkedUnrevealed;
        this.ifRevealAtStart = ifRevealAtStart;
    }

}