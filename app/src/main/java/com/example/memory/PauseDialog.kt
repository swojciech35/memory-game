
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.example.memory.R

class PauseDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater;
            builder.setView(inflater.inflate(R.layout.pause_dialog_layout,null))
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
        return super.onCreateDialog(savedInstanceState)
    }





}