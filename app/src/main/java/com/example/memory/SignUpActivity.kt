package com.example.memory

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import com.example.memory.databinding.ActivitySignUpBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class SignUpActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySignUpBinding
    private lateinit var auth: FirebaseAuth

    private lateinit var progressDialog: ProgressDialog
    private lateinit var actionBar: ActionBar
    private lateinit var database: DatabaseReference
    private var email=""
    private var password=""
    private var repeatPassword=""
    private var nick=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val actionBar=supportActionBar
        actionBar!!.hide()

        progressDialog= ProgressDialog(this)
        progressDialog.setTitle("Please wait")
        progressDialog.setMessage("Register...")
        progressDialog.setCanceledOnTouchOutside(false)

        auth= FirebaseAuth.getInstance()
        if(checkForInternet(this)) {
            binding.signUpBtn.setOnClickListener {
                validateData()
            }
        }else{
            Toast.makeText(this, "no internet connection", Toast.LENGTH_SHORT).show()
        }


    }

    private fun validateData() {
        email=binding.emailEt.text.toString().trim()
        password=binding.passwordEt.text.toString().trim()
        repeatPassword=binding.repeatPasswordEt.text.toString().trim()
        nick=binding.nickEt.text.toString().trim()
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            binding.emailEt.error="Invalid email format"
        }else if(TextUtils.isEmpty(password)){
            binding.passwordEt.error="Please enter password"
        }else if(password.length<6){
            binding.passwordEt.error="Password must atleast 6 chacters long"
        }
        else{
            if(password==repeatPassword){
                firebaseSignUp()
            }else{
                Toast.makeText(this, "Passwords do not match", Toast.LENGTH_SHORT).show()
            }

        }
    }

    private fun firebaseSignUp() {
        progressDialog.show()

        auth.createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                progressDialog.dismiss()
                val firebaseUser=auth.currentUser
                val email=firebaseUser!!.email
                Toast.makeText(this, "Account created $email", Toast.LENGTH_SHORT).show()
                createUserInDataBase(firebaseUser.uid,nick,email.toString())
                val intent= Intent(applicationContext,ChooseActivity::class.java)
                intent.putExtra("id",firebaseUser.uid)
//                intent.putExtra("email",email)//uid- id
                startActivity(intent)
                finish()
            }
            .addOnFailureListener { e->
                Toast.makeText(this, "SignUp Failed ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
    fun createUserInDataBase(id: String,nick:String,email:String){
        saveData("UserIdSharedPref",id)
        saveDataInt("LevelSharedPref",1)
        saveDataInt("ScoreSharedPref",0)
        database=FirebaseDatabase.getInstance("https://memorygame-443e4-default-rtdb.europe-west1.firebasedatabase.app").getReference("Users")
        val User=User(id,nick,email,1,0)
        database.child(id).setValue(User).addOnSuccessListener {
            Toast.makeText(this, "add to database", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this,  "error database", Toast.LENGTH_SHORT).show()
        }
    }
    private fun checkForInternet(context: Context):Boolean{
        val connectivityManager=context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){
            val network=connectivityManager.activeNetwork ?: return false
            val activeNetwork=connectivityManager.getNetworkCapabilities(network) ?: return false

            return when{
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ->true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)-> true

                else ->false
            }
        }else{
            @Suppress("Depraction") val networkInfo=connectivityManager.activeNetworkInfo ?: return false
            @Suppress("Depracton") return networkInfo.isConnected
        }
    }
    private fun saveData(identyficator : String, key:String){
        val sharedPreferences=getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        val editor=sharedPreferences.edit()
        editor.apply{

            putString(identyficator,key)
        }.apply()
    }
    private fun saveDataInt(identyficator : String, key:Int){
        val sharedPreferences=getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        val editor=sharedPreferences.edit()
        editor.apply{
            putInt(identyficator,key)
        }.apply()
    }
}